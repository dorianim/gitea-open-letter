To support this open letter, please add your name at the bottom of this page and [send a pull request](https://codeberg.org/SocialCoding/gitea-open-letter/pulls).

---


Dear Gitea Owners,

In the name of the Gitea Community who elected you last year[^0], we welcome the creation of a for-profit company that allows you to make a living out of Gitea.

But there has been some confusion as to the ownership of the Gitea domains and trademarks, which are essential parts of the project. As a Community, we trusted you to care for them because they are one of the most important assets any Free Software project has (if not the most important).

We believed you when you promised to pass along the ownership of the Gitea project to your elected successors. This promise is part of an essential bond between you and the strong Community of volunteers, as well as all those who rely upon our collective efforts.

With that in mind, **you can understand our surprise when we learned on October 25th, 2022[^1] that both the domains and the trademark were transferred to a for-profit company without our knowledge or approval.**

**We want to believe this is an honest mistake and ask that:**

* **A non-profit organisation owned by the Gitea community is created.**
* **The Gitea trademark and domains are transferred to the non-profit.**
* **The name of the company is changed to avoid any confusion with the non-profit.**

These are steps that are needed to help restore the trust we, as Community, have given to the Gitea project. A new non-profit organisation also gives us opportunities to improve and ensure a healthy future, for instance by:

- Implementing an intuitive and fair election process.
- Describing the ways in which democratic decisions are to be made.
- Providing accessible places where all relevant information can be found.
- Establishing a [DoOcracy](https://communitywiki.org/wiki/DoOcracy) that works and continue to improve it.

And explore even more ambitious ideas: it is for the whole Community to imagine the future of the organisation and make it a reality.

### We await your response

In summary, the announcement of the for-profit company was not well received and this reflects poorly on the entire Gitea Community. **We need to restore a positive image as quickly as possible: please provide us with your answer in the next few days** so we can move forward again. 

Your forever enthusiastic Gitea Community.


[^0]: [https://github.com/go-gitea/gitea/blob/main/CONTRIBUTING.md#owners](https://github.com/go-gitea/gitea/blob/main/CONTRIBUTING.md#owners)



[^1]: [https://blog.gitea.io/2022/10/open-source-sustainment-and-the-future-of-gitea/](https://blog.gitea.io/2022/10/open-source-sustainment-and-the-future-of-gitea/)


---

Signatures:

* Loïc Dachary
* Aravinth Manivannan
* André Jaenisch (Ryuno-Ki)
* Arnold Schrijver
* Anthony Wang
* Johannes Schnürle
* Otto Richter
* Gusted
* Oliverpool
* Beowulf / BeoCode
* Yarmo Mackenbach
* Federico Leva
* Datta Adithya
* Scott Williams
* Felipe L. Sologuren G.
* Pierre Ozoux
* Jae Lo Presti
* Daniel [REDACTED] <pjals.envs.net>
* Caesar Schinas
* Norwin / noerw
* LeJun
* Aminda Suomalainen
* [James Pearson](https://jamespearson.xyz)
* Stefano Costa (stekoo)
* [Tommi](https://tommi.space)
* [Fabio Manganiello](https://fabiomanganiello.com)
* [Citizen4](https://gitea.citizen4.eu)
* [Akib Azmain Turja](https://codeberg.org/akib)
* [mose](https://mose.com)
* [rdelaage](https://rdelaage.ovh)
* Antonino Catinello
* [Dorian Zedler](https://dorian.im)